import React, { Component } from 'react';
import {
    AppRegistry,
    StatusBar
} from 'react-native';

import App from './src/App';

export default class omtsx extends Component {
    componentWillMount() {
        StatusBar.setBarStyle('light-content', true);
    }
    render() {
        return (
            <App />
        );
    }
}

AppRegistry.registerComponent('main', () => omtsx);
