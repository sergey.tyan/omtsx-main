import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
var styles = require('../style.js');

const TouchableElement =
    Platform.OS === 'android' ? TouchableOpacity : TouchableHighlight;
export default class ControlPanel extends Component {
    popView() {
        Actions.pop();
    }

    render() {
        return (
            <View style={styles.navList}>
                <View style={styles.leftNav}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={
                            this.props.dismiss
                                ? this.props.dismiss
                                : this.popView
                        }
                    >
                        <Text style={styles.textLeft}>Отменить</Text>
                    </TouchableElement>
                </View>

                <View style={styles.navTitle}>
                    <Text style={styles.navTitleTextBold}>
                        {this.props.title}
                    </Text>
                </View>

                <View style={styles.rightNav}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={this.props.send}
                    >
                        <Text style={styles.textLeft}>
                            {this.props.sendText
                                ? this.props.sendText
                                : 'Отправить'}
                        </Text>
                    </TouchableElement>
                </View>
            </View>
        );
    }
}
