import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    Image,
    Animated,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import {
    openBest,
    openMain,
    openRandom,
    openNotifications,
    openCategories,
    openSettings,
    openMessages,
    openProfile
} from '../redux/actions';
import {
    OPEN_MAIN,
    OPEN_BEST,
    OPEN_RANDOM,
    OPEN_NOTIFICATIONS,
    OPEN_CATEGORIES,
    OPEN_SETTINGS,
    OPEN_MESSAGES
} from '../redux/actions/types';
import styles from '../style.js';
import { width } from '../constants';
import { UserData } from '../models/UserData';

export class ControlPanel extends Component {
    constructor(props: any) {
        super(props);

        this.state = {
            search: false
        };
    }

    componentWillMount() {
        this.animatedSearchBox = new Animated.Value(width - 90);
        this.animatedSearchCancel = new Animated.Value(0);
    }

    enableSearch() {
        this.props.onSearch();
        this.state.search = true;
        Animated.timing(this.animatedSearchBox, {
            toValue: width - 120,
            duration: 300
        }).start();

        Animated.timing(this.animatedSearchCancel, {
            toValue: 1,
            duration: 300
        }).start();
    }

    disableSearch() {
        this.props.outSearch();
        this.state.search = false;
        Animated.timing(this.animatedSearchBox, {
            toValue: width - 75,
            duration: 300
        }).start();

        Animated.timing(this.animatedSearchCancel, {
            toValue: 0,
            duration: 300
        }).start();
    }

    render() {
        const TouchableElement =
            Platform.OS === 'android'
                ? TouchableNativeFeedback
                : TouchableHighlight;
        const animatedSearchBoxStyle = { width: this.animatedSearchBox };
        const animatedSearchCancelStyle = {
            opacity: this.animatedSearchCancel
        };

        return (
            <View
                style={this.state.search ? styles.leftSearch : styles.leftMenu}
            >
                <View style={styles.searchBox}>
                    <Animated.View
                        style={[styles.searchBar, animatedSearchBoxStyle]}
                    >
                        <TextInput
                            ref="searchInput"
                            style={
                                this.state.search
                                    ? styles.searchInputActive
                                    : styles.searchInput
                            }
                            onFocus={this.enableSearch.bind(this)}
                            onBlur={this.disableSearch.bind(this)}
                            placeholder="Поиск..."
                            placeholderTextColor={
                                this.state.search ? '#bca6a8' : '#9ea8b5'
                            }
                            underlineColorAndroid="transparent"
                        />
                        <Icon
                            name={'md-search'}
                            style={styles.searchIcon}
                            size={16}
                            color={this.state.search ? '#bca6a8' : '#9ea8b5'}
                        />
                    </Animated.View>

                    <Animated.View
                        style={[styles.cancelBtn, animatedSearchCancelStyle]}
                    >
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => {
                                this.refs.searchInput.blur();
                                this.refs.searchInput.setNativeProps({
                                    text: ''
                                });
                            }}
                        >
                            <Text style={[styles.textCancel]}>Отмена</Text>
                        </TouchableElement>
                    </Animated.View>
                </View>

                <View
                    style={
                        this.state.search ? styles.nothing : styles.menuItems
                    }
                >
                    <View style={styles.menuProfile}>
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => this.props.openProfile()}
                        >
                            <View style={styles.avatarProfile}>
                                <View style={styles.imgProfile}>
                                    <Image
                                        source={{
                                            uri:
                                                this.props.currentUser
                                                    .photoURL ||
                                                'https://test.ru/'
                                        }}
                                        style={styles.itemImage}
                                    />
                                </View>

                                <View style={styles.nameProfile}>
                                    <Text style={styles.nameProfileText}>
                                        {this.props.currentUser.displayName ||
                                            'username placeholder'}
                                    </Text>
                                </View>
                            </View>
                        </TouchableElement>

                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_MAIN
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={() =>
                                    this.props.openMain(this.props.item)}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_MAIN
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Главная
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_BEST
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={() =>
                                    this.props.openBest(this.props.item)}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_BEST
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Лучшие
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_RANDOM
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={this.props.openRandom}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_RANDOM
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Случайные
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_MESSAGES
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={this.props.openMessages}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_MESSAGES
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Сообщения
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <View style={styles.notificationCount}>
                                <Text style={styles.nCountText}>1</Text>
                            </View>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_NOTIFICATIONS
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={this.props.openNotifications}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_NOTIFICATIONS
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Уведомления
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_CATEGORIES
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={this.props.openCategories}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_CATEGORIES
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Категории
                                </Text>
                            </TouchableElement>
                        </View>
                        <View style={styles.menuItem}>
                            <Icon
                                name={'md-home'}
                                style={styles.menuIcon}
                                size={22}
                                color={
                                    this.props.item === OPEN_SETTINGS
                                        ? '#5b080a'
                                        : '#8d9aaa'
                                }
                            />
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={this.props.openSettings}
                            >
                                <Text
                                    style={
                                        this.props.item === OPEN_SETTINGS
                                            ? styles.textItemActive
                                            : styles.textItem
                                    }
                                >
                                    Настройки
                                </Text>
                            </TouchableElement>
                        </View>
                    </View>
                </View>

                <View
                    style={
                        this.state.search ? styles.searchRes : styles.nothing
                    }
                >
                    <View style={styles.searchItem}>
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => {}}
                        >
                            <Text style={styles.searchItemText}>В лесу</Text>
                        </TouchableElement>
                    </View>
                </View>
                <View
                    style={
                        this.state.search ? styles.searchRes : styles.nothing
                    }
                >
                    <View style={styles.searchItem}>
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => {}}
                        >
                            <Text style={styles.searchItemText}>
                                В комнате моей было
                            </Text>
                        </TouchableElement>
                    </View>
                </View>
                <View
                    style={
                        this.state.search ? styles.searchRes : styles.nothing
                    }
                >
                    <View style={styles.searchItem}>
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => {}}
                        >
                            <Text style={styles.searchItemText}>
                                В доме у моего брата
                            </Text>
                        </TouchableElement>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = ({ sideMenu, auth }) => {
    const { item } = sideMenu;
    const { displayName, photoURL } = auth.user;
    const userData = new UserData();
    userData.displayName = displayName;
    userData.photoURL = photoURL;
    return {
        item,
        currentUser: userData
    };
};

export default connect(mapStateToProps, {
    openBest,
    openMain,
    openRandom,
    openNotifications,
    openCategories,
    openSettings,
    openMessages,
    openProfile
})(ControlPanel);
