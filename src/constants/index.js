import { Dimensions } from 'react-native';

export const mainColor = '#663332';
export const mainColor2 = '#70403e';
export const chatBubbleColor = '#773942';
export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;
export const NOTIFICATION_TYPE = {
    story_accepted: 'story_accepted',
    story_declined: 'story_declined',
    post_like: 'post_like',
    post_comment: 'post_comment',
    mention: 'mention'
};

export const NOTIFICATION_DESCRIPTION = {
    story_accepted: {
        action: 'Ваша запись успешно опубликована',
        icon: null
    },
    story_declined: {
        action: 'Ваша запись отклонена',
        icon: null
    },
    post_like: {
        action: 'оценил(-а) вашу запись',
        icon: 'ios-heart'
    },
    post_comment: {
        action: 'написал(-а) комментарий',
        icon: 'md-text'
    },
    mention: {
        action: 'отметил(-а) вас на комментарии',
        icon: 'md-person-add'
    }
};

export const TIME_FORMAT = 'YYYY.MM.DD HH:mm';
