import { Actions } from 'react-native-router-flux';
import C from './types';
import { AsyncStorage } from 'react-native';
import * as firebase from 'firebase';
import { getFilteredCategories, getNotifications } from './UserInfoActions';

export const emailChanged = text => {
    return {
        type: C.EMAIL_CHANGED,
        payload: text
    };
};

export const passwordChanged = text => {
    return {
        type: C.PASSWORD_CHANGED,
        payload: text
    };
};

export const loginUser = ({ email, password }) => {
    return dispatch => {
        dispatch({
            type: C.LOGIN_USER_SUCCESS,
            payload: null
        });

        // const value = AsyncStorage.getItem('@OMTSX:firstLaunch')
        //     .then(() => {
        //         if (value !== null) {
        //             Actions.main();
        //         } else {
        //             AsyncStorage.setItem('@OMTSX:firstLaunch', true)
        //                 .then(() => Actions.slider())
        //                 .catch((error) => alert(error));
        //         }
        //
        //     })
        //     .catch((error) => alert(error));

        // Actions.main();

        dispatch({ type: C.LOGIN_USER });

        // firebase.auth().signInWithEmailAndPassword("sergey.tyan2@gmail.com", '12345678')
        // firebase.auth().signInWithEmailAndPassword("alex@mail.ru", '12345678')
        firebase
            .auth()
            .signInWithEmailAndPassword('sergey.tyan.iitu@gmail.com', '12345678')
            .then(user => loginUserSuccess(dispatch, user))
            .catch(error => {
                alert(error);
                firebase
                    .auth()
                    .createUserWithEmailAndPassword(email, password)
                    .then(user => loginUserSuccess(dispatch, user))
                    .catch(() => loginUserFail(dispatch));
            });
    };
};

const loginUserFail = dispatch => {
    dispatch({ type: C.LOGIN_USER_FAIL });
};

const loginUserSuccess = (dispatch, user) => {
    dispatch(getNotifications(user.uid, 8));
    dispatch(getFilteredCategories(user.uid, 8));
    dispatch({
        type: C.LOGIN_USER_SUCCESS,
        payload: user
    });

    Actions.main();
};
