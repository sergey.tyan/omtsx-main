import C from './types';
import * as firebase from 'firebase';
import { uploadAvatarPromise } from './photoUploadService';
import { UserData } from '../../models/UserData';
import { Notification } from '../../models/Notification';

export const getUserInfo = uid => {
    const approvedPostsRef = firebase.database().ref(C.F_USER_INFO + uid);
    return dispatch => {
        dispatch({ type: C.LOAD_USER_INFO, payload: uid });
        approvedPostsRef.on('value', snapshot => {
            dispatch({
                type: C.USER_INFO_LOADED,
                payload: snapshot.val()
            });
        });
    };
};

export const stopWatchingUserInfo = uid => {
    return () => {
        firebase
            .database()
            .ref(C.F_USER_INFO + uid)
            .off();
    };
};

export const editUserPhoto = (user, imageUri) => {
    return dispatch => {
        if (imageUri) {
            dispatch({
                type: C.PROFILE_UPLOADING_PHOTO
            });
            const uploadPromise = uploadAvatarPromise(user.uid, imageUri);
            const userRef = firebase
                .database()
                .ref()
                .child(C.F_USER_INFO + user.uid);
            uploadPromise
                .then(url => {
                    userRef.update({
                        photoURL: url
                    });
                    user
                        .updateProfile({
                            photoURL: url
                        })
                        .then(() => {
                            dispatch({
                                type: C.PROFILE_PHOTO_UPDATED
                            });
                        });
                })
                .catch(err => {
                    alert('Cant upload image ' + err);
                    dispatch({
                        type: C.PROFILE_PHOTO_UPDATED
                    });
                });
        }
    };
};

export const editUserBackgroundPhoto = (user, imageUri) => {
    return dispatch => {
        if (imageUri) {
            dispatch({
                type: C.PROFILE_UPLOADING_PHOTO
            });
            const uploadPromise = uploadAvatarPromise(user.uid, imageUri);
            const userRef = firebase
                .database()
                .ref()
                .child(C.F_USER_INFO + user.uid);
            uploadPromise
                .then(url => {
                    userRef
                        .update({
                            backgroundPhotoURL: url
                        })
                        .then(() => {
                            dispatch({
                                type: C.PROFILE_PHOTO_UPDATED
                            });
                        });
                })
                .catch(err => {
                    alert('Cant upload image ' + err);
                    dispatch({
                        type: C.PROFILE_PHOTO_UPDATED
                    });
                });
        }
    };
};

export const editUserData = (user, data: UserData) => {
    return () => {
        const userRef = firebase
            .database()
            .ref()
            .child(C.F_USER_INFO + user.uid);

        user.updateProfile({
            displayName: data.displayName
        });

        userRef.update({
            displayName: data.displayName,
            about: data.about,
            gender: data.gender
        });
    };
};

export const pushNotification = (notificationReceiverUid, notification: Notification) => {
    const notificationsRef = firebase.database().ref(C.F_USER_NOTIFICATIONS + notificationReceiverUid);

    notificationsRef.push(notification);
};

export const getNotifications = (uid, count) => {
    const notificationsRef = firebase
        .database()
        .ref(C.F_USER_NOTIFICATIONS + uid)
        .limitToLast(count);
    return dispatch => {
        notificationsRef.on('value', snapshot => {
            dispatch({
                type: C.NEW_NOTIFICATION,
                payload: snapshot.val()
            });
        });
    };
};

export const getFilteredCategories = uid => {
    const ref = firebase.database().ref(`user-info/${uid}`);

    return dispatch => {
        ref.on('value', snapshot => {
            const filtersRaw = snapshot.val()['categories-filter'];
            if (filtersRaw !== null) {
                const activeCategories = filtersRaw.split(',');
                dispatch({
                    type: C.FILTERED_CATEGORIES_LOADED,
                    payload: activeCategories
                });
            }
        });
    };
};
