import * as firebase from 'firebase';
import RNFetchBlob from 'react-native-fetch-blob';
import { Platform } from 'react-native';
const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

export const uploadAvatarPromise = (uid, imageUri) =>{
    if(imageUri) {
        return new Promise((resolve, reject) => {
            let uploadBlob = null;
            const imageRef = firebase.storage().ref().child('user-avatars/' + uid);
            imageUri = Platform.OS === 'ios' ? imageUri.replace('file://', '') : imageUri

            const mime = 'application/octet-stream';
            fs.readFile(imageUri, 'base64')
                .then((data) => {
                    return Blob.build(data, {type: `${mime};BASE64`})
                })
                .then((blob) => {
                    uploadBlob = blob;
                    return imageRef.put(blob, {contentType: mime})
                })
                .then(() => {
                    uploadBlob.close();
                    return imageRef.getDownloadURL()
                })
                .then((url) => {
                    resolve(url)
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }
};

export const uploadChatImagePromise = (messageKey, imageUri) =>{
    if(imageUri) {
        return new Promise((resolve, reject) => {
            let uploadBlob = null;
            const imageRef = firebase.storage().ref().child('chat-images/' + messageKey);
            imageUri = Platform.OS === 'ios' ? imageUri.replace('file://', '') : imageUri

            console.log(imageUri);
            const mime = 'application/octet-stream';
            fs.readFile(imageUri, 'base64')
                .then((data) => {
                    return Blob.build(data, {type: `${mime};BASE64`})
                })
                .then((blob) => {
                    uploadBlob = blob;
                    return imageRef.put(blob, {contentType: mime})
                })
                .then(() => {
                    uploadBlob.close();
                    return imageRef.getDownloadURL()
                })
                .then((url) => {
                    resolve(url)
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }
};