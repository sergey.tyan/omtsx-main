import C from '../actions/types';
import { INITIAL_STATE_AUTH } from '../initialState';


export default (state = INITIAL_STATE_AUTH, action) => {
    switch (action.type) {
        case C.EMAIL_CHANGED:
            return {...state, email: action.payload};
        case C.PASSWORD_CHANGED:
            return {...state, password: action.payload};
        case C.LOGIN_USER:
            return {...state, loading: true, error: ''};
        case C.LOGIN_USER_SUCCESS:
            return {...state, ...INITIAL_STATE_AUTH, user: action.payload};
        case C.LOGIN_USER_FAIL:
            return {...state, error: 'Authentication Failed.', password: '', loading: false};
        default:
            return state;
    }
};
