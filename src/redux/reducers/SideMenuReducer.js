import {
    OPEN_BEST,
    OPEN_CATEGORIES,
    OPEN_MAIN,
    OPEN_NOTIFICATIONS,
    OPEN_PROFILE,
    OPEN_RANDOM,
    OPEN_SETTINGS,
    OPEN_MESSAGES
} from '../actions/types';

const INITIAL_STATE = {
    item:OPEN_MAIN
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case OPEN_BEST:
            return { ...state, item: action.type};
        case OPEN_CATEGORIES:
            return { ...state, item: action.type };
        case OPEN_MAIN:
            return { ...state, item: action.type };
        case OPEN_NOTIFICATIONS:
            return { ...state, item: action.type };
        case OPEN_PROFILE:
            return { ...state, item: action.type };
        case OPEN_RANDOM:
            return { ...state, item: action.type };
        case OPEN_SETTINGS:
            return { ...state, item: action.type };
        case OPEN_MESSAGES:
            return { ...state, item: action.type };
        default:
            return state;
    }
};
