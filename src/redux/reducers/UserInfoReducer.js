import C from '../actions/types';
import { INITIAL_STATE_USER_INFO } from '../initialState';

export default (state = INITIAL_STATE_USER_INFO, action) => {
    switch (action.type) {
        case C.LOAD_USER_INFO: {
            return {
                ...state,
                userId: action.payload,
                loading: true
            };
        }
        case C.USER_INFO_LOADED: {
            if (action.payload) {
                return {
                    ...state,
                    displayName: action.payload.displayName,
                    photoURL: action.payload.photoURL,
                    backgroundPhotoURL: action.payload.backgroundPhotoURL,
                    moderationPostsCount: action.payload.moderationPostsCount,
                    likes: action.payload.likes,
                    about: action.payload.about,
                    loading: false
                };
            } else {
                return {
                    ...state
                };
            }
        }
        case C.PROFILE_UPLOADING_PHOTO: {
            return {
                ...state,
                loading: true
            };
        }
        case C.PROFILE_PHOTO_UPDATED: {
            return {
                ...state,
                loading: false
            };
        }
        case C.LIKE: {
            return {
                ...state,
                likes: action.payload
            };
        }
        case C.REMOVE_LIKE: {
            return {
                ...state,
                likes: action.payload
            };
        }
        case C.NEW_NOTIFICATION: {
            return {
                ...state,
                notifications: action.payload
            };
        }
        case C.FILTERED_CATEGORIES_LOADED: {
            return {
                ...state,
                filteredCategories: action.payload
            };
        }
        default: {
            return state;
        }
    }
};
