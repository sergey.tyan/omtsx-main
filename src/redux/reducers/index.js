import {combineReducers} from "redux";
import AuthReducer from "./AuthReducer";
import SideMenuReducer from "./SideMenuReducer";
import PostsReducer from "./PostsReducer";
import CommentsReducer from "./CommentsReducer";
import CatReducer from "./CatReducer";
import RegistrationReducer from "./RegistrationReducer";
import ChatReducer from "./ChatReducer";
import UserInfoReducer from "./UserInfoReducer";

export default combineReducers({
    auth: AuthReducer,
    sideMenu: SideMenuReducer,
    posts: PostsReducer,
    comments: CommentsReducer,
    categories: CatReducer,
    registration: RegistrationReducer,
    chat: ChatReducer,
    userInfo: UserInfoReducer
});
