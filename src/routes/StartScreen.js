import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

const StartScreen = () => {
    return (
        <View style={styles.container}>
            <ScrollView style={{ marginTop: 80 }}>
                <Text style={styles.button} onPress={() => Actions.login()}>
                    Вход
                </Text>

                <Text
                    style={styles.button}
                    onPress={() => Actions.registration()}
                >
                    Регистрация
                </Text>

                <Text style={styles.button} onPress={() => Actions.forgot()}>
                    Забыли пароль?
                </Text>
                <Text style={styles.button} onPress={() => Actions.slider()}>
                    Слайдер
                </Text>
                <Text style={styles.button} onPress={() => Actions.news()}>
                    Страница новостей
                </Text>

                <Text style={styles.button} onPress={() => Actions.single()}>
                    Случайная запись
                </Text>

                <Text
                    style={styles.button}
                    onPress={() => Actions.notifications()}
                >
                    Уведомления
                </Text>

                <Text style={styles.button} onPress={() => Actions.edit()}>
                    Редактирование
                </Text>

                <Text style={styles.button} onPress={() => Actions.new()}>
                    Новая публикация
                </Text>
                <Text style={styles.button} onPress={() => Actions.comments()}>
                    Комментарии
                </Text>
                <Text
                    style={styles.button}
                    onPress={() => Actions.categories()}
                >
                    Категории
                </Text>
                <Text style={styles.button} onPress={() => Actions.account()}>
                    Аккаунт
                </Text>
                <Text style={styles.button} onPress={() => Actions.user()}>
                    Пользователь
                </Text>

                <Text style={styles.button} onPress={() => Actions.catselect()}>
                    Выбор категории
                </Text>

                <Text style={styles.button} onPress={() => Actions.settings()}>
                    Настройки
                </Text>

                <Text
                    style={styles.button}
                    onPress={() => Actions.subscribers()}
                >
                    Подписчики
                </Text>

                <Text style={styles.button} onPress={() => Actions.faq()}>
                    Вопросы и ответы
                </Text>

                <Text style={styles.button} onPress={() => Actions.agreement()}>
                    Пользовательское соглашение
                </Text>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        padding: 10,
        width: 230,
        textAlign: 'center',
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#663332',
        fontSize: 14,
        color: '#663332',
        marginBottom: 10
    }
});

export default StartScreen;
