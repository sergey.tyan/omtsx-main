/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform,
    ListView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import { connect } from 'react-redux';
import axios from 'axios';
import C from '../../redux/actions/types';
import * as firebase from 'firebase';
import PostItem from './PostItem';
import styles from '../../style.js';
import NavBar from '../../components/navBar';

const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;

class SingleRandomPost extends Component {

    loadKeysAndRandomPost() {

        if (this.state.keys.length === 0) {
            axios.get('https://blinding-heat-7224.firebaseio.com/posts/posted.json?shallow=true')
                .then(res => {
                    let keys = Object.keys(res.data);
                    let randomKey = _.sample(keys);
                    this.setState({keys, randomKey}, this.loadPost);
                })
                .catch(err => console.log(err));
        } else {
            let randomKey = _.sample(this.state.keys);
            this.setState({randomKey}, this.loadPost);

        }
    }

    loadPost() {
        firebase.database().ref(C.F_POSTED_POSTS + this.state.randomKey).once('value', snapshot => {
            const post = {
                ...snapshot.val(),
                postId: this.state.randomKey
            };
            this.createDataSource(post);
        });
    }

    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            keys: []
        };

        this.onNextPost = this.onNextPost.bind(this);
        this.renderItem = this.renderItem.bind(this);
        this.loadPost = this.loadPost.bind(this);
        this.loadKeysAndRandomPost();
    }

    renderItem(item) {
        return (<PostItem item={item} currentUser={this.props.currentUser} afterLike={this.loadPost}/>)
    }


    render() {
        return (

            <View style={styles.containerNews}>

                <NavBar openDrawer={this.props.openDrawer.bind(this)}/>

                <ListView
                    scrollbarStyle='outsideOverlay'
                    dataSource={this.state.dataSource}
                    renderRow={this.renderItem}
                    style={styles.listS}
                />
                <View style={styles.bottomBtn}>
                    <TouchableElement
                        style={styles.nextBtn}
                        underlayColor='transparent'
                        onPress={this.onNextPost}
                    >
                        <Text style={styles.nextText}>Следующая запись</Text>
                    </TouchableElement>
                    <Icon style={styles.nextArrow} color='#fff' name={'ios-arrow-forward'} size={22}/>
                </View>
            </View>
        );
    }

    onNextPost() {
        this.loadKeysAndRandomPost();
    }

    createDataSource(post) {
        let item = [post];

        this.setState({dataSource: this.state.dataSource.cloneWithRows(item)});
    }
}

const mapStateToProps = (state) => {
    return {currentUser: state.auth.user};
};

export default connect(mapStateToProps)(SingleRandomPost);