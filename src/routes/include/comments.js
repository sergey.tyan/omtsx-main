import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
    ListView,
    LayoutAnimation,
    Image,
    KeyboardAvoidingView,
    TextInput,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import moment from 'moment';
import * as firebase from 'firebase';
import Hr from 'react-native-hr';

require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru'); //set moment local language to zh-cn

import { width } from '../../constants';
import styles from '../../style.js';
import Spinner from '../../components/Spinner';
import { fetchCommentsByPost, postComment, stopWatchingCommentsByPost, commentTyped } from '../../redux/actions';

const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

let CommentView = KeyboardAvoidingView;
let androidCommentInput = {};
let androidCommentInputContainer = {};
if (Platform.OS === 'android') {
    CommentView = View;
    androidCommentInput = { height: null, marginLeft: 0 };
    androidCommentInputContainer = { padding: 5, height: null };
}

const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
});

const commentStyle = StyleSheet.create({
    mentionItem: {
        height: 40,
        width: width - 14,
        backgroundColor: 'white',
        flexDirection: 'row',
        padding: 5
    },
    image: {
        width: 30,
        height: 30,
        marginRight: 5
    }
});

class Comments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeComment: -1,
            commentCount: 10,
            mentionListVisible: false,
            mentionDataSource: ds.cloneWithRows([])
        };
        this.lastWord = '';
        this.mentionedUsers = {};
        this.onListViewEndReached = this.onListViewEndReached.bind(this);
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    selectComment(rowID) {
        LayoutAnimation.easeInEaseOut();
        this.setState({ activeComment: rowID });
    }

    renderLoading() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }
    }

    openProfile(userId) {
        Actions.accountPage({ userId });
    }

    renderRow = (item, sectionID, rowID) => {
        const postingTime = moment(item.postTime, 'YYYY.MM.DD HH:mm').fromNow(false);

        return (
            <View style={styles.itemNComments}>
                <View style={styles.contentComment}>
                    <TouchableOpacity
                        underlayColor="rgba(0,0,0,0.1)"
                        onPress={() => this.selectComment(rowID)}
                        style={styles.commentBtn}
                    >
                        <View />
                    </TouchableOpacity>

                    <View style={styles.nAvatar}>
                        <View style={styles.imgProfileN}>
                            <Image source={{ uri: item.userImg }} style={styles.itemImageN} />
                        </View>
                    </View>

                    <View style={styles.nDesc}>
                        <TouchableElement onPress={() => this.openProfile(item.userId)}>
                            <View style={styles.headN}>
                                <Text style={item.userId !== '' ? styles.nameItemN : styles.nothing}>
                                    {item.userName}{' '}
                                </Text>
                            </View>
                        </TouchableElement>

                        <Text style={item.body !== '' ? styles.txtCommentItemN : styles.nothing}>{item.body} </Text>

                        <Text style={styles.footNCom}>
                            <Text style={styles.timeItemN}>{postingTime} </Text>
                            <Text style={item.likes !== '' ? styles.likes : styles.nothing}> {item.likes}</Text>
                        </Text>
                    </View>
                </View>

                <View style={styles.actionsComment}>
                    {/*<View style={(rowID === this.state.activeComment) ? styles.cAction : styles.nothing}>*/}
                    {/*<TouchableOpacity*/}
                    {/*underlayColor='transparent'*/}
                    {/*onPress={() => {alert(postingTime)}}*/}
                    {/*>*/}
                    {/*<Icon name={'ios-more'} color='#8c98ab' style={styles.contextIcon} size={22}/>*/}
                    {/*</TouchableOpacity>*/}

                    {/*</View>*/}
                    <View style={rowID === this.state.activeComment ? styles.cAction : styles.nothing}>
                        <TouchableOpacity
                            underlayColor="transparent"
                            onPress={() => {
                                this.props.commentTyped(`@${item.displayName}`);
                                this.refs.textInput.focus();
                            }}
                        >
                            <Icon name={'ios-undo'} color="#8c98ab" style={styles.contextIcon} size={22} />
                        </TouchableOpacity>
                    </View>
                    {/*<View style={(rowID === this.state.activeComment) ? styles.cAction : styles.nothing}>*/}
                    {/*<TouchableOpacity*/}
                    {/*underlayColor='transparent'*/}
                    {/*onPress={() => {}}*/}
                    {/*>*/}
                    {/*<Icon name={'md-thumbs-down'} color='#8c98ab' style={styles.contextIcon} size={22}/>*/}
                    {/*</TouchableOpacity>*/}

                    {/*</View>*/}
                    <View style={rowID === this.state.activeComment ? styles.cAction : styles.nothing}>
                        <TouchableOpacity underlayColor="transparent" onPress={() => {}}>
                            <Icon name={'md-thumbs-up'} color="#8c98ab" style={styles.contextIcon} size={22} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };

    postComment() {
        const mentionedUsers = this.props.currentComment
            .split(' ')
            .filter(possibleUser => this.mentionedUsers[possibleUser] !== undefined)
            .map(possibleUser => this.mentionedUsers[possibleUser]);

        console.log(mentionedUsers);
        const comment = {
            userName: this.props.user.displayName.trim().toLowerCase(),
            userId: this.props.user.uid,
            userImg: this.props.user.photoURL,
            body: this.props.currentComment,
            postId: this.props.postId,
            mentionedUsers
        };
        this.props.postComment(comment);
    }

    onEnterMention = text => {
        firebase
            .database()
            .ref('user-info')
            .orderByChild('displayName')
            .startAt(text)
            .endAt(text + '\uf8ff')
            .limitToFirst(5)
            .once('value', item => {
                const items = _.map(item.val(), (user, uid) => ({
                    uid,
                    photoURL: user.photoURL,
                    displayName: user.displayName
                }));

                this.setState({ mentionDataSource: this.dataSource.cloneWithRows(items) });

                console.log(items);
            });
    };

    mentionSelected = item => {
        const commentText = this.props.currentComment.replace(new RegExp(`${this.lastWord}$`), `@${item.displayName}`);
        this.props.commentTyped(commentText.trim());
        this.mentionedUsers[`@${item.displayName.trim()}`] = item;
        this.setState({ mentionListVisible: false });
        this.refs.textInput.focus();
    };

    renderMentionItem = item => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.mentionSelected(item)}>
                    <View style={commentStyle.mentionItem}>
                        {item.photoURL && <Image source={{ uri: item.photoURL }} style={commentStyle.image} />}
                        <Text>{item.displayName}</Text>
                    </View>
                </TouchableOpacity>
                <Hr lineColor={'#f2f2f2'} />
            </View>
        );
    };

    onChangeText = text => {
        const words = text.split(' ');
        const lastWord = words[words.length - 1];
        if (lastWord.startsWith('@')) {
            if (lastWord.length > 1) {
                if (text.endsWith(' ')) {
                    this.setState({ mentionListVisible: false });
                } else {
                    this.setState({ mentionListVisible: true });
                    this.lastWord = lastWord;
                    this.onEnterMention(lastWord.slice(1));
                }
            }
        } else {
            if (this.state.mentionListVisible) {
                this.setState({ mentionListVisible: false });
            }
        }
        this.props.commentTyped(text);
    };

    render() {
        return (
            <CommentView style={styles.containerNews} behavior={'padding'}>
                {this.props.comments.length === 0 && <Text style={{ marginTop: 10 }}>Комментариев нет</Text>}
                {!this.state.mentionListVisible && (
                    <ListView
                        dataSource={this.dataSource}
                        renderRow={this.renderRow}
                        style={styles.listN}
                        enableEmptySections
                        onEndReached={this.onListViewEndReached}
                    />
                )}
                {this.state.mentionListVisible && (
                    <ListView
                        dataSource={this.state.mentionDataSource}
                        enableEmptySections
                        renderRow={this.renderMentionItem}
                        style={styles.listN}
                    />
                )}
                {this.renderLoading()}
                <View style={[styles.commentInputContainer, androidCommentInputContainer]}>
                    <TextInput
                        ref="textInput"
                        style={[styles.commentInput, androidCommentInput]}
                        placeholder="Комментарий..."
                        placeholderTextColor="#c7aeb2"
                        underlineColorAndroid="transparent"
                        onChangeText={this.onChangeText}
                        value={this.props.currentComment}
                    />
                    <TouchableOpacity
                        underlayColor="#ccc"
                        onPress={this.postComment.bind(this)}
                        style={styles.sendCommentButtonContainer}
                    >
                        <View style={styles.sendCommentButton}>
                            <Text style={styles.sendCommentButtonText}>Отпр.</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </CommentView>
        );
    }

    onListViewEndReached() {
        this.setState({ commentCount: this.state.commentCount + 10 }, () =>
            this.props.fetchCommentsByPost(this.props.postId, this.state.commentCount)
        );
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    createDataSource({ comments }) {
        this.dataSource = ds.cloneWithRows(comments);
    }

    componentWillMount() {
        this.props.fetchCommentsByPost(this.props.postId, this.state.commentCount);
        this.createDataSource(this.props);
    }

    componentWillUnmount() {
        this.props.stopWatchingCommentsByPost(this.props.postId);
    }
}

const mapStateToProps = state => {
    let comments = [];

    if (state.comments) {
        comments = _.map(state.comments.comments, (val, commentId) => {
            return { ...val, commentId };
        });
    }

    return {
        currentComment: state.comments.currentComment,
        comments: comments,
        loading: state.comments.loading,
        user: state.auth.user
    };
};

export default connect(mapStateToProps, {
    fetchCommentsByPost,
    postComment,
    stopWatchingCommentsByPost,
    commentTyped
})(Comments);
