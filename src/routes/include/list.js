/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { View, StatusBar, Platform, ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { fetchPublishedPosts, stopWatchingPublishedPosts } from '../../redux/actions';
import PostItem from './PostItem';
import Spinner from '../../components/Spinner';
import NavBar from '../../components/navBar';

import * as styles from '../../style.js';

class PostsList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            postCount: 20,
            posts: [],
            dataSource: ds.cloneWithRows({}),
            filteredCategories: []
        };
    }

    renderItem(item) {
        return <PostItem item={item} currentUser={this.props.currentUser} />;
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    renderLoading() {
        if (this.props.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar
                    title={this.props.title}
                    openDrawer={this.props.openDrawer.bind(this)}
                    rightButtonAction={() => Actions.newPost()}
                />

                <View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                        onEndReached={() => {
                            this.setState({ postCount: this.state.postCount + 5 }, () =>
                                this.props.fetchPublishedPosts(this.state.postCount)
                            );
                        }}
                    />

                    {this.renderLoading()}
                </View>
            </View>
        );
    }

    componentWillReceiveProps(nextProps) {
        const possibleNewPosts = JSON.stringify(nextProps.posts);
        const loadNewPosts = this.state.posts !== possibleNewPosts;
        if (loadNewPosts) {
            const posts = nextProps.posts.filter(post => {
                if (
                    post.category &&
                    nextProps.filteredCategories &&
                    nextProps.filteredCategories.indexOf(String(post.category.id)) !== -1
                ) {
                    return false;
                }
                return true;
            });

            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(posts),
                posts: possibleNewPosts
            });
        }
    }

    componentWillMount() {
        this.props.fetchPublishedPosts(this.state.postCount);
    }

    componentWillUnmount() {
        this.props.stopWatchingPublishedPosts();
    }
}

const mapStateToProps = state => {
    let posts = _.map(state.posts.posted, (val, postId) => {
        return { ...val, postId };
    });
    posts = _.orderBy(posts, ['postingTime']).reverse();
    let result = {
        posts,
        loading: state.posts.loading,
        currentUser: state.auth.user,
        filteredCategories: state.userInfo.filteredCategories
    };
    return result;
};

export default connect(mapStateToProps, {
    fetchPublishedPosts,
    stopWatchingPublishedPosts
})(PostsList);
