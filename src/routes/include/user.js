/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TouchableNativeFeedback,
  Platform,
  Animated,
  Dimensions,
  ListView,
  TouchableOpacity
} from 'react-native';

import Hr from 'react-native-hr';
import Icon from 'react-native-vector-icons/Ionicons';
import Image from 'react-native-image-progress';

var ParallaxView = require('react-native-parallax-view');
var width = Dimensions.get('window').width;
var styles = require('../../style.js');
import NavBar from '../../components/navBarAccount';
import Modal from '../../components/modalPicker';

const items = [
{
  name: 'w.kz',
  slogan: 'Рекламная запись',
  cat: '',
  url: '',
  title: 'Лучшее  белье в Казахстане',
  text: 'Is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industry stadard dummy text...',
  image: { uri:'https://placeimg.com/75/75/any' },
  mainImage: { uri:'https://image.ibb.co/igEhbF/news.png' } ,
  likes: 0,
  comments: 5
},
{
  name: 'w.kz',
  slogan: 'Рекламная запись',
  cat: '',
  url: '',
  title: 'Лучшее  белье в Казахстане',
  text: 'Is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industry stadard dummy text...',
  image: { uri:'https://placeimg.com/75/75/any' },
  mainImage: { uri:'https://image.ibb.co/igEhbF/news.png' } ,
  likes: 0,
  comments: 5
},
{
  name: 'w.kz',
  slogan: 'Рекламная запись',
  cat: '',
  url: '',
  title: 'Лучшее  белье в Казахстане',
  text: 'Is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industry stadard dummy text...',
  image: { uri:'https://placeimg.com/75/75/any' },
  mainImage: { uri:'https://image.ibb.co/igEhbF/news.png' } ,
  likes: 0,
  comments: 5
}
];

export default class omtsx extends Component {

  constructor(props) {
      super(props);

      this.state = {
        dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2}),
        textInputValue: ''
      };
      

  }
  componentWillMount() {

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(items)
    });
  }
  renderItem(item) {
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
     TouchableElement = TouchableNativeFeedback;
    }
      return(

        <View style={styles.itemProfileNews}>

              <View  style={((item.name != '')) ? styles.headItem : styles.nothing} >
                <View style={styles.imgItem}>
                  <Image source={{ uri: item.image.uri }} style={styles.itemImage} />
                </View>
                <View style={styles.titleItemWr}>
                  <Text style={styles.titleItem}>{item.name}</Text>
                  <Text style={styles.catItem}>{item.slogan}</Text>

                </View>
                <Modal />

                 
              </View>

            
              <View style={styles.itemMain}>

                  <Image source={require('../../images/news.png')} style={((item.mainImage.uri != '')) ? styles.itemMainImage : styles.nothing} />

   
              </View>

              <View style={((item.url != '')) ? styles.itemLink : styles.nothing} >
                <Text style={styles.textLink}>
                  Перейти на сайт
                </Text>
                
                  <Icon style={styles.rightArrow}  color='#fff' name={"ios-arrow-forward"} size={16}/>

                
                

              </View>

              <View style={styles.content}>
                  <Text style={styles.titleContent}>
                      {item.title}
                  </Text>
                  <Text style={styles.textContent}>
                       {item.text}
                  </Text>
                  <TouchableElement
                  underlayColor='transparent'
                  onPress={() => {}}>
                  <Text style={styles.readMore}>
                       Показать полностью...
                  </Text>
                  </TouchableElement>
                  <Hr style={styles.hr} lineColor='#f6f2fa'  />

                  <View style={styles.soc}>

                    
                      <Icon style={styles.likesIcon} color={((item.likes > 0)) ? "#653430": "#c1c4c9"}  name={((item.likes > 0)) ? "ios-heart": "ios-heart-outline"} size={25}/>
                      <Text style={styles.likesCount}>{item.likes}</Text>
                    

                    
                      <Icon style={styles.commentsIcon} color='#c1c4c9' name={"md-chatboxes"} size={25}/>
                      <Text style={styles.commentsCount}>{item.comments}</Text>


                      <Text style={((item.cat != '')) ? styles.category : styles.nothing } >
                        {item.cat}
                      </Text>
                
               

              
                 

                  </View>

              </View>
              
           

           



        </View>
      )
  }

  componentDidMount() {

  }


  render() {
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
     TouchableElement = TouchableNativeFeedback;
    }
    return (

      <View style={styles.containerNews}>
       
        <NavBar rightIcon='md-text' title='@janettsoy' openDrawer={this.props.openDrawer.bind(this)} />
        <ParallaxView
            backgroundSource={{uri: 'https://placeimg.com/'+width+'/215/any'}}
            windowHeight={215}
            scrollableViewStyle={styles.viewUser}
            header={(
            <View style={styles.listProfileHeader}>
              <View style={styles.userMainImage}>
              <Image style={(Platform.OS === 'android') ? styles.userBg : styles.nothing} source={{uri: 'https://placeimg.com/300/215/any'}} />
                <View style={(Platform.OS === 'android') ? styles.accountInfoOne : styles.accountInfo}>
                  <View style={styles.accountAvatar}>
                    <Image style={styles.accountAvatarImage} source={{uri: 'https://placeimg.com/100/100/any'}} />
                  </View>
                  <View style={styles.accountNick}>
                    <Text style={styles.accountNickText}>alibek123</Text>
                  </View>
                  <View style={styles.accountStatus}>
                    <Text style={styles.accountStatusText}>Моя ночь светлее твоего дня</Text>
                  </View>
                  <View style={styles.editButton}>
                    <TouchableOpacity
                    underlayColor='rgba(0,0,0,0.1)'
                    style={styles.commentBtn}
                    onPress={()=>{ 
                     }} >
                     <View></View>
                    </TouchableOpacity>
                    <Icon color="#7889a0" name='md-checkmark' style={styles.editIcon} size={10}/>
                    <Text style={styles.editButtonText}>Подписка</Text>

                  </View>
                </View>
              </View>

              

            </View>
           )}
        >
        <View style={styles.profileCounts}>
                 <View style={styles.profileCount}>
                  <Text style={styles.profileCountText}>12 записей</Text>
                </View>              
                <View style={styles.profileCount}>
                  <Text style={styles.profileCountText}>3 подписчики</Text>
                </View>          

                <View style={styles.profileCount}>
                  <Text style={styles.profileCountText}>0 подписки</Text>
                </View>          
              
              </View>
        <ListView
            scrollEnabled={false}
            dataSource={this.state.dataSource}
            renderRow={this.renderItem.bind(this)}
            style={styles.listProfile}


        />
      
      </ParallaxView>
      </View>

      
    )
  }
}

